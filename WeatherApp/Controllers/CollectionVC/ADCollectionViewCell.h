//
//  ADCollectionViewCell.h
//  WeatherApp
//
//  Created by Anton on 3/5/17.
//  Copyright © 2017 Anton Duda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *weatherIconImage;
@property (weak, nonatomic) IBOutlet UILabel *tempMaxLabel;

@property (weak, nonatomic) IBOutlet UILabel *tempMinLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;



@end
