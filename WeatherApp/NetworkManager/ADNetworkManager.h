//
//  ADNetworkManager.h
//  WeatherApp
//
//  Created by Anton on 3/5/17.
//  Copyright © 2017 Anton Duda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADNetworkManager : NSObject

+ (id)sharedManager;


@end
