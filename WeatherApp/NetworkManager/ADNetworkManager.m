//
//  ADNetworkManager.m
//  WeatherApp
//
//  Created by Anton on 3/5/17.
//  Copyright © 2017 Anton Duda. All rights reserved.
//

#import "ADNetworkManager.h"

@implementation ADNetworkManager


+ (id)sharedManager {
    static ADNetworkManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

@end
