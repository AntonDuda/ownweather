//
//  AppDelegate.h
//  WeatherApp
//
//  Created by Anton on 3/2/17.
//  Copyright © 2017 Anton Duda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

